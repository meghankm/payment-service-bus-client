package com.meghan.paymentconfigbusclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentconfigbusclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentconfigbusclientApplication.class, args);
	}

}
